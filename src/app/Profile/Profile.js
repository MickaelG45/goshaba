import React from 'react';
import { shape } from 'prop-types';
import { Container, Text, Content } from 'native-base';
import Header from '../shared/Header/Header';
import Footer from '../shared/Footer/Footer';

class Profile extends React.Component {
	static navigationOptions = {
		header: <Header title="Profile" />,
	};
	render() {
		return (
			<Container>
				<Content>
					<Text>
						Welcome to Profile Page
					</Text>
				</Content>
				<Footer navigate={this.props.navigation.navigate} />
			</Container>
		);
	}
}

Profile.propTypes = {
	navigation: shape().isRequired,
};

export default Profile;
