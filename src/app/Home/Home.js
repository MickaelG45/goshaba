import React from 'react';
import { shape } from 'prop-types';
import { Container, Text, Tab, Tabs, TabHeading } from 'native-base';
import Header from '../shared/Header/Header';
import BasicInfo from './Forms/BasicInfo/BasicInfo';
import Jobs from './Forms/Jobs/Jobs';
import Skills from './Forms/Skills/Skills';

class Home extends React.Component {
	static navigationOptions = {
		header: <Header title="Home" />,
	};
	render() {
		return (
			<Container>
				<Tabs>
					<Tab heading={<TabHeading><Text>Basic Info</Text></TabHeading>}>
						<BasicInfo />
					</Tab>
					<Tab
						heading={
							<TabHeading>
								<Text>Jobs</Text>
							</TabHeading>
						}
					>
						<Jobs />
					</Tab>
					<Tab heading={<TabHeading><Text>Skills</Text></TabHeading>}>
						<Skills />
					</Tab>
				</Tabs>
			</Container>
		);
	}
}

Home.propTypes = {
	navigation: shape().isRequired,
};

export default Home;
