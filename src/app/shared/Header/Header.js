import { Header, Title, Body } from 'native-base';
import { string } from 'prop-types';
import React from 'react';

const MyHeader = ({ title }) => (
	<Header hasTabs>
		<Body>
			<Title>{title}</Title>
		</Body>
	</Header>
);

MyHeader.propTypes = {
	title: string.isRequired,
};

export default MyHeader;
