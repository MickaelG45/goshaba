import React from 'react';
import { Button, Footer, Badge, Icon, FooterTab, Text } from 'native-base';
import { func } from 'prop-types';

const MyFooter = ({ navigate }) => (
	<Footer>
		<FooterTab>
			<Button active badge vertical onPress={() => navigate('Home')}>
				<Badge>
					<Text>2</Text>
				</Badge>
				<Icon active name="apps" />
				<Text>Apps</Text>
			</Button>
			<Button vertical onPress={() => navigate('Profile')}>
				<Icon name="camera" />
				<Text>Camera</Text>
			</Button>
			<Button badge vertical>
				<Badge>
					<Text>51</Text>
				</Badge>
				<Icon name="navigate" />
				<Text>Navigate</Text>
			</Button>
			<Button vertical>
				<Icon name="person" />
				<Text>Contact</Text>
			</Button>
		</FooterTab>
	</Footer>
);

MyFooter.propTypes = {
	navigate: func.isRequired,
};

export default MyFooter;
