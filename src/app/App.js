import { StackNavigator } from 'react-navigation';
import HomeScreen from './Home/Home';
import ProfileScreen from './Profile/Profile';

const App = StackNavigator({
	Home: { screen: HomeScreen },
	Profile: { screen: ProfileScreen },
});

export default App;
